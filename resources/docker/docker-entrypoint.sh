#!/bin/bash
set -e
echo "[ ****************** ] Starting Endpoint of Application"
chmod +x /var/www/html/post-install.sh
/usr/bin/supervisord
/usr/bin/crontab
echo "[ ****************** ] Ending Endpoint of Application"
exec "$@"
