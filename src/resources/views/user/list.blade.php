<?php
?>
@extends('adminlte::page')

@section('title', 'Listagem de Usuários')

@section('content_header')
    <h1>Listagem de Usuários</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <table id="table_id" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th>Nível</th>
                    <th>Ação</th>
                </tr>
                </thead>

            </table>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css" />
@stop
@section('plugins.Moment', true)
@section('plugins.Datatables', true)
@section('plugins.Bootbox', true)
@section('plugins.Select2', true)
@section('plugins.Mask', true)
@section('plugins.Validate', true)
@section('js')
    <script src="{{url('js/user/list.js')}}"></script>
@stop
