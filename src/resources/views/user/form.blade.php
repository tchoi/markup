<?php
use \Illuminate\Support\Facades\Auth;
$user = Auth::user();

?>
<form id="user-form">
    <input type="hidden" class="form-control" id="id" name="id" value="{{$id ?? ""}}" />
    <div class="form-group">
        <label for="name">Nome:</label>
        <input type="text" class="form-control" id="name" name="name" value="{{$name ?? ""}}" required />
    </div>
    <div class="form-group">
        <label for="email">E-mail:</label>
        <input type="email" class="form-control" id="email" name="email" value="{{$email ?? ""}}" required/>
    </div>
    <div class="form-group">
        <label for="password">Senha:</label>
        <input type="password" class="form-control" id="password" name="password" {{!isset($id) ? "required" : ""}} />
    </div>
	@if ($user->level === \App\Services\UserService::LEVEL_ADMIN)
    <div class="form-group">
        <label for="level">Nivel:</label>
        <select id="level" name="level" placeholder="Selecione um nível" class="form-control select2-bootstrap" required>
            <option value=""></option>
            <option value="ADMIN" {{isset($level) && $level == \App\Services\UserService::LEVEL_ADMIN ? "selected" : "" }}>ADMINISTRATIVO</option>
            <option value="USER" {{isset($level) && $level == \App\Services\UserService::LEVEL_USER ? "selected" : ""}}>USUÁRIO</option>
        </select>
    </div>
    @else
    <input type="hidden" name="level" value="{{$level}}" />
    @endif
</form>
