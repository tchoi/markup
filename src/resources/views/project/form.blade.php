<?php

use Illuminate\Support\Facades\Auth;

$userLogged = Auth::user();
$addressObj = isset($address) ? json_decode($address, true) : [];
$contactPointObj = isset($contact_point) ? json_decode($contact_point, true) : [];
?>
<form id="project-form">
    <input type="hidden" class="form-control" id="id" name="id" value="{{$id ?? ""}}" />
    @if ($userLogged->level == \App\Services\UserService::LEVEL_ADMIN)
        <div class="form-group">
            <label for="user_id">Usuário:</label>
            <select id="user_id" name="user_id" placeholder="Selecione um usuário" class="form-control select2-bootstrap" required>
                <option value=""></option>
                @foreach($users as $user)
                    <option value="{{$user->id}}" {{isset($user_id) && $user->id == $user_id ? "selected" : "" }}>{{$user->name}}</option>
                @endforeach
            </select>
        </div>
    @else
        <input type="hidden" class="form-control" id="id" name="id" value="{{isset($user_id) ? $user_id : $userLogged->id}}" />
    @endif
    <div class="form-group">
        <label for="name">Nome:</label>
        <input type="text" class="form-control" id="name" name="name" value="{{$name ?? ""}}" required />
    </div>
    <div class="form-group">
        <label for="legal_name">Nome Legal:</label>
        <input type="text" class="form-control" id="legal_name" name="legal_name" value="{{$legal_name ?? ""}}" required />
    </div>
    <div class="form-group">
        <label for="founding_date">Ano de Fundação:</label>
        <input type="text" class="form-control" id="founding_date" name="founding_date" maxlength="4" value="{{$founding_date ?? ""}}" required />
    </div>
    <div class="form-group">
        <label for="description">Descrição:</label>
        <textarea class="form-control" rows="2" maxlength="170" name="description" id="description" placeholder="Descrição">{{$description ?? ""}}</textarea>
    </div>
    <div class="form-group">
        <label for="logo">Logo:</label>
        <input type="text" class="form-control" id="logo" name="logo" value="{{$logo ?? ""}}" required/>
    </div>
    <div class="form-group">
        <label for="url">URL principal (colocar barra no final):</label>
        <input type="url" class="form-control" id="url" name="url" value="{{$url ?? ""}}" required/>
    </div>
    <div class="form-group">
        <label for="same_as">URL secundárias: (uma por linha e com barra no final de cada url)</label>
        <textarea class="form-control" rows="5" name="same_as" id="same_as" placeholder="Urls">{{$same_as ?? ""}}</textarea>
    </div>
    <div class="form-group">
        <label for="in_language">Lingua:</label>
        <input type="text" class="form-control" id="in_language" name="in_language" placeholder="pt-BR" maxlength="5" value="{{$in_language ?? ""}}" />
    </div>

    <div class="form-group">
        <label for="founders">Fundadores (um por linha):</label>
        <textarea class="form-control" rows="5" name="founders" id="founders" placeholder="Fundadores">{{$founders ?? ""}}</textarea>
    </div>

    <div class="form-group">
        <label for="address_street">Endereço:</label>
        <input type="text" class="form-control" id="address_street" name="address[street]" placeholder="Endereço" value="{{$addressObj['street'] ?? ""}}" />
    </div>

    <div class="form-row">
        <div class="form-group col-6">
            <label for="address_city">Cidade:</label>
            <input type="text" class="form-control" id="address_city" name="address[city]" placeholder="Cidade" value="{{$addressObj['city'] ?? ""}}" />
        </div>
        <div class="form-group col-6">
            <label for="address_state">Estado:</label>
            <input type="text" class="form-control" id="address_state" name="address[state]" placeholder="Estado" value="{{$addressObj['state'] ?? ""}}" />
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-6">
            <label for="address_postalcode">CEP:</label>
            <input type="text" class="form-control" id="address_postalcode" name="address[postalcode]" placeholder="CEP" value="{{$addressObj['postalcode'] ?? ""}}" />
        </div>
        <div class="form-group col-6">
            <label for="address_country">País:</label>
            <input type="text" class="form-control" id="address_country" name="address[country]" placeholder="País" value="{{$addressObj['country'] ?? ""}}" />
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-4">
            <label for="contact_point_telephone">Tel. de Contato:</label>
            <input type="tel" class="form-control" id="contact_point_telephone" name="contact_point[telephone]" placeholder="Telefone de contato" value="{{$contactPointObj['telephone'] ?? ""}}" />
        </div>
        <div class="form-group col-4">
            <label for="contact_point_email">E-mail de Contato:</label>
            <input type="email" class="form-control" id="contact_point_email" name="contact_point[email]" placeholder="Estado" value="{{$contactPointObj['email'] ?? ""}}" />
        </div>
        <div class="form-group col-4">
            <label for="contact_point_type">Tipo do Contato:</label>
            <input type="text" class="form-control" id="contact_point_type" name="contact_point[type]" placeholder="Tipo" value="{{$contactPointObj['type'] ?? ""}}" />
        </div>
    </div>
</form>
