<?php

use Illuminate\Support\Facades\Auth;

$userLogged = Auth::user();
?>
<form id="marker-form-webpage">
    <input type="hidden" class="form-control" id="id" name="id" value="{{$id ?? ""}}" />
    <input type="hidden" class="form-control" id="type" name="type" value="WEBPAGE" />

    <div class="form-group">
        <label for="project_id">Projeto:</label>
        <select id="project_id" name="project_id" placeholder="Selecione um projeto" class="form-control select2-bootstrap" required>
            <option value=""></option>
            @foreach($projects as $project)
                <option value="{{$project->id}}" {{isset($project_id) && $project->id == $project_id ? "selected" : "" }}>{{$project->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="name">Nome:</label>
        <input type="text" class="form-control" id="name" name="name" value="{{$name ?? ""}}" required />
    </div>
    <div class="form-group">
        <label for="description">Descrição:</label>
        <textarea class="form-control" rows="2" maxlength="170" name="description" id="description" placeholder="Descrição">{{$description ?? ""}}</textarea>
    </div>
    <div class="form-group">
        <label for="url">URL principal (colocar barra no final):</label>
        <input type="url" class="form-control" id="url" name="url" value="{{$url ?? ""}}" required/>
    </div>
    <div class="form-group">
        <label for="in_language">Lingua:</label>
        <input type="text" class="form-control" id="in_language" name="in_language" placeholder="pt-BR" maxlength=5" value="{{$in_language ?? ""}}" required />
    </div>

    <div class="form-group">
        <label for="image">Url da Imagem Principal:</label>
        <input type="url" class="form-control" id="image" name="image" value="{{$image ?? ""}}" />
    </div>

    <div class="form-group">
        <label for="width">Largura da Imagem Principal:</label>
        <input type="number" class="form-control" id="width" name="width" value="{{$width ?? ""}}" />
    </div>
    <div class="form-group">
        <label for="height">Altura da Imagem Principal:</label>
        <input type="number" class="form-control" id="height" name="height" value="{{$height ?? ""}}" />
    </div>
    <div class="form-group">
        <label for="caption">Caption da Imagem Principal:</label>
        <input type="text" class="form-control" id="caption" name="caption" value="{{$caption ?? ""}}" />
    </div>
    <div class="form-group">
        <label for="breadcrumbs">Breadcrumbs: (uma por linha, no formato titulo, url)<br />* Não é necessário repetir o valor da URL principal</label>
        <textarea class="form-control" rows="5" name="breadcrumbs" id="breadcrumbs" placeholder="Ex.: Home, http://www.teste.com.br">{{$breadcrumbs ?? ""}}</textarea>
    </div>
</form>
