<?php

use Illuminate\Support\Facades\Auth;

$userLogged = Auth::user();
?>
<form id="marker-form-faq">
    <input type="hidden" class="form-control" id="id" name="id" value="{{$id ?? ""}}" />
    <input type="hidden" class="form-control" id="type" name="type" value="FAQ" />

    <div class="form-group">
        <label for="project_id">Projeto:</label>
        <select id="project_id" name="project_id" placeholder="Selecione um projeto" class="form-control select2-bootstrap" required>
            <option value=""></option>
            @foreach($projects as $project)
                <option value="{{$project->id}}" {{isset($project_id) && $project->id == $project_id ? "selected" : "" }}>{{$project->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="name">Nome:</label>
        <input type="text" class="form-control" id="name" name="name" value="{{$name ?? ""}}" required />
    </div>

    <div class="form-group">
        <label for="url">URL principal (colocar barra no final):</label>
        <input type="url" class="form-control" id="url" name="url" value="{{$url ?? ""}}" required/>
    </div>

    <div class="form-group">
        <label for="FAQ">Perguntas e Respostas: (uma por linha, no formato pergunta@@resposta)</label>
        <textarea class="form-control" rows="5" name="faqs" id="faqs" placeholder="Ex.: Quem é voce?@@Sou eu">{{$faqs ?? ""}}</textarea>
    </div>
</form>
