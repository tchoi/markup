
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <p>Welcome to admin panel to Schema Markup.</p>
    <hr>
    <p>
        <b>Schema markup</b> ( schema.org) is a structured data vocabulary that helps search engines better understand the info on your website in order to serve rich results. These markups allow search engines to see the meaning and relationships behind entities mentioned on your site.
    </p>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css" />
@stop

@section('plugins.Moment', true)
@section('plugins.Datatables', true)
@section('plugins.Bootbox', true)
@section('plugins.Select2', true)
@section('plugins.Mask', true)
@section('plugins.Validate', true)

@section('js')

@stop
