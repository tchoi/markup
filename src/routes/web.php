<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('welcome')->middleware('auth');

Auth::routes();

Route::get('/home', function() {
    return view('home');
})->name('home')->middleware('auth');


Route::group(['prefix' => 'user', 'middleware' => 'auth'], function () {
    Route::get('list', "UserController@index");
    Route::post('get-list', 'UserController@getList');
    Route::get('form', 'UserController@form');
    Route::post('store', 'UserController@store');
    Route::put('edit/{id}', 'UserController@edit');
    Route::delete('delete/{id}', 'UserController@delete');
});

Route::group(['prefix' => 'project', 'middleware' => 'auth'], function () {
    Route::get('list', "ProjectController@index");
    Route::post('get-list', 'ProjectController@getList');
    Route::get('form', 'ProjectController@form');
    Route::post('store', 'ProjectController@store');
    Route::put('edit/{id}', 'ProjectController@edit');
    Route::delete('delete/{id}', 'ProjectController@delete');
    Route::get('check-create', "ProjectController@checkIfPermitProject");
    Route::get('view-ldjson', "ProjectController@getLDJson");
});

Route::group(['prefix' => 'marker', 'middleware' => 'auth'], function () {
    Route::get('list', "MarkerController@index");
    Route::post('get-list', 'MarkerController@getList');
    Route::get('form', 'MarkerController@form');
    Route::post('store', 'MarkerController@store');
    Route::put('edit/{id}', 'MarkerController@edit');
    Route::delete('delete/{id}', 'MarkerController@delete');
    Route::get('view-ldjson', "MarkerController@getLDJson");
});
