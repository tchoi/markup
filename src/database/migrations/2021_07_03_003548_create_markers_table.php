<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('markers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("project_id");
            $table->string("type", 255);
            $table->string("name", 255);
            $table->text("description")->nullable();
            $table->string("url", 255)->nullable();
            $table->dateTimeTz("date_published")->nullable();
            $table->dateTimeTz("date_modified")->nullable();
            $table->longText("breadcrumbs")->nullable();
            $table->longText("faqs")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('markers');
    }
}
