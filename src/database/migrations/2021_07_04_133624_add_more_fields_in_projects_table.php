<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoreFieldsInProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->string("legal_name", 255)->nullable()->after("name");
            $table->string("founding_date", 4)->nullable()->after("logo");
            $table->text("founders")->nullable()->after("founding_date");
            $table->text("address")->nullable()->after("founders");
            $table->text("contact_point")->nullable()->after("address");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            //
        });
    }
}
