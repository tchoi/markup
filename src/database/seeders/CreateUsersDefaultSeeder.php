<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CreateUsersDefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Jonas Mendes Ferreira",
            'email' => "inboxfox@gmail.com",
            'email_verified_at' => now(),
            'password' => Hash::make("destino2002"), // password
            'remember_token' => Str::random(10),
            "level" => "ADMIN"
        ]);
        DB::table('users')->insert([
            'name' => "Hugo Calixto",
            'email' => "hugo.calixto@supergestao.com",
            'email_verified_at' => now(),
            'password' => Hash::make("hugocalixto"), // password
            'remember_token' => Str::random(10),
            "level" => "ADMIN"
        ]);
    }
}
