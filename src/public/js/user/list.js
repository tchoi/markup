function removeItem(id)
{
    bootbox.confirm({
        message: "Deseja mesmo excluir esse usuário?",
        buttons: {
            confirm: {
                label: 'Sim',
                className: 'btn-success'
            },
            cancel: {
                label: 'Não',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                var type = "DELETE";
                var url = "/user/delete/"+id;
                var formdata = null;
                var contentType = "application/x-www-form-urlencoded";
                var processData = true;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:"json",
                    type: type,
                    url: url,
                    data: formdata, // serializes the form's elements.
                    contentType: contentType,
                    processData: processData,
                    cache: false,
                    success: function(data){
                        bootbox.hideAll();
                        tableUser.ajax.reload();
                    },
                    error: function(data){

                    }
                });
            }
        }
    });
}

var tableUser;
$(document).ready( function () {
    tableUser = $('#table_id').DataTable({
        buttons: [
            {
                text: 'Novo Usuário',
                action: function ( e, dt, node, config ) {
                    getFormUser("");
                }
            }
        ],
        dom: 'Bfrtip',
        lengthChange: false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json"
        },
        processing: true,
        serverSide: true,
        ajax: {
            url:'/user/get-list',
            "type": "POST",
            "beforeSend" : function(xhr) {
                var token = $('meta[name="csrf-token"]').attr('content');
                xhr.setRequestHeader('X-CSRF-TOKEN',token);
            },
            data: function (d) {
                //d.razao_social = $('#frm-filter #razao_social').val();
            }
        },

        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'level', name: 'level'},
            {data: 'acao', name: 'acao'},
        ],
        "order": [[ 1, 'asc' ]],
        "fnDrawCallback": function (data, type, full, meta) {

        }
    });
    tableUser.buttons().container().appendTo( '#table_id_wrapper .col-md-6:eq(0)' );

    $(document).off('click', '.editar');
    $(document).on('click', '.editar', function () {
        getFormUser($(this).data('id'));
    });


    $(document).off('click', '.remover');
    $(document).on('click', '.remover', function () {
        removeItem($(this).data('id'));
    });
} );
