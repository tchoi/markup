function getSubForm()
{
    bootbox.dialog({
        title: "Tipo de Marcador",
        message: "Selecione o tipo de marcador",
        closeButton: true,
        scrollable: true,
        size: 'large',
        buttons: {
            webpage: {
                label: 'WEBPAGE',
                className: 'btn-primary',
                callback: function () {
                    getForm("", "WEBPAGE")
                }
            },
            faq: {
                label: 'FAQ',
                className: 'btn-warning',
                callback: function () {
                    getForm("", "FAQ")
                }
            },
            cancelar: {
                label: 'Cancelar',
                className: 'btn-danger',
                callback: function () {
                    return true;
                }
            }
        }
    });
}

function getForm(id, typeMarker)
{
    var verb = "POST";
    var urlForm = "/marker/store"

    if (id != "") {
        verb = "PUT";
        urlForm = "/marker/edit/" + id
    }


    $.get("/marker/form", {cache: false, "id": id, "type": typeMarker}, function ( data ) {
        var formName = "#marker-form-" + typeMarker.toLowerCase();
        bootbox.dialog({
            title: "Cadastro de marcador",
            message: data,
            closeButton: true,
            scrollable: true,
            size: 'large',
            buttons: {
                comprar: {
                    label: 'Ok',
                    className: 'btn-primary',
                    callback: function () {
                        if (!$(formName).validate().form()) {
                            return false;
                        }

                        var type = verb;
                        var url = urlForm
                        var formdata = null;
                        var contentType = "application/x-www-form-urlencoded";
                        var processData = true;
                        if ($(formName).attr('enctype') === 'multipart/form-data') {
                            formdata = new FormData($("#user-form").get(0));//seleciona classe form-horizontal adicionada na tag form do html
                            contentType = false;
                            processData = false;
                        } else {
                            formdata = $(formName).serialize();
                        }

                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:"json",
                            type: type,
                            url: url,
                            data: formdata, // serializes the form's elements.
                            contentType: contentType,
                            processData: processData,
                            cache: false,
                            success: function (data) {
                                bootbox.hideAll();
                                table.ajax.reload();
                            },
                            error: function (data) {

                            }
                        });
                        return false;
                    }
                },
                cancelar: {
                    label: 'Cancelar',
                    className: 'btn-danger',
                    callback: function () {
                        return true;
                    }
                }
            }
        });
    });

}

function removeItem(id)
{
    bootbox.confirm({
        message: "Deseja mesmo excluir esse projeto?",
        buttons: {
            confirm: {
                label: 'Sim',
                className: 'btn-success'
            },
            cancel: {
                label: 'Não',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                var type = "DELETE";
                var url = "/marker/delete/" + id;
                var formdata = null;
                var contentType = "application/x-www-form-urlencoded";
                var processData = true;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:"json",
                    type: type,
                    url: url,
                    data: formdata, // serializes the form's elements.
                    contentType: contentType,
                    processData: processData,
                    cache: false,
                    success: function (data) {
                        bootbox.hideAll();
                        table.ajax.reload();
                    },
                    error: function (data) {

                    }
                });
            }
        }
    });
}

function viewLDJson(id)
{
    $.get( "/marker/view-ldjson", {cache: false, "id": id},function( data ) {
        bootbox.dialog({
            title: "LD-Json Gerado",
            message: data,
            closeButton: true,
            scrollable: true,
            size: 'large',
            buttons: {
                copy_and_paste: {
                    label: 'Copiar Script',
                    className: 'btn-primary',
                    callback: function() {
                        (async () => {
                            await navigator.clipboard.writeText($("#ldjson-form #ld_json").text());
                        })();
                    }
                },
                cancelar: {
                    label: 'Cancelar',
                    className: 'btn-danger',
                    callback: function() {
                        return true;
                    }
                }
            }
        }).find("div.modal-dialog").css({ "width": "100%", "height": "100%" });
    });
}

var table;
$(document).ready(function () {
    table = $('#table_id').DataTable({
        buttons: [
            {
                text: 'Novo marcador',
                action: function ( e, dt, node, config ) {
                    getSubForm();
                }
            }
        ],
        dom: 'Bfrtip',
        lengthChange: false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json"
        },
        processing: true,
        serverSide: true,
        ajax: {
            url:'/marker/get-list',
            "type": "POST",
            "beforeSend" : function (xhr) {
                var token = $('meta[name="csrf-token"]').attr('content');
                xhr.setRequestHeader('X-CSRF-TOKEN',token);
            },
            data: function (d) {
                //d.razao_social = $('#frm-filter #razao_social').val();
            }
        },

        columns: [
            {data: 'id', name: 'id'},
            {data: 'project_name', name: 'project_name'},
            {data: 'type', name: 'type'},
            {data: 'name', name: 'name'},
            {data: 'acao', name: 'acao'},
        ],
        "order": [[ 1, 'asc' ]],
        "fnDrawCallback": function (data, type, full, meta) {

        }
    });
    table.buttons().container().appendTo('#table_id_wrapper .col-md-6:eq(0)');

    $(document).off('click', '.editar');
    $(document).on('click', '.editar', function () {
        getForm($(this).data('id'), $(this).data("typemarker"));
    });


    $(document).off('click', '.remover');
    $(document).on('click', '.remover', function () {
        removeItem($(this).data('id'));
    });

    $(document).off('click', '.ld_json_viewer');
    $(document).on('click', '.ld_json_viewer', function () {
        viewLDJson($(this).data('id'));
    });
});
