jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "currency-pre": function ( a ) {
        var cur = jQuery.trim(a)=="" ? "0,00" : a;
        cur = a.toString().replace(".", "").replace(",", ".");
        //a = (a==="-") ? 0 : a.replace( /[^\d\-\.]/g, "" );
        return parseFloat( cur );
    },

    "currency-asc": function ( a, b ) {
        return a - b;
    },

    "currency-desc": function ( a, b ) {
        return b - a;
    },
    "extractdate-pre": function(value) {
        console.log(value);
        date = moment(value, "YYYY-MM-DD HH:mm:ss").unix();
        console.log(date);
        return date;
    },
    "extractdate-asc": function(a, b) {
        return a - b;
    },
    "extractdate-desc": function(a, b) {
        return b - a;
    },
    "date-euro-pre": function ( a ) {
        var x;
        if ( $.trim(a) !== '' ) {
            var frDatea = $.trim(a).split(' ');
            var frTimea = (undefined != frDatea[1]) ? frDatea[1].split(':') : [0,0,0];

            var frDatea2 = frDatea[0].split('/');
            x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + ((undefined != frTimea[2]) ? frTimea[2] : 0)) * 1;

            console.log(x);
        }
        else {
            x = Infinity;
        }

        return x;
    },

    "date-euro-asc": function ( a, b ) {
        return a - b;
    },

    "date-euro-desc": function ( a, b ) {
        return b - a;
    }
});
