function getForm(id)
{
    var verb = "POST";
    var urlForm = "/project/store"

    if (id != "") {
        verb = "PUT";
        urlForm = "/project/edit/" + id
    }
    $.get( "/project/form", {cache: false, "id": id},function( data ) {
        bootbox.dialog({
            title: "Cadastro de projeto",
            message: data,
            closeButton: true,
            scrollable: true,
            size: 'large',
            buttons: {
                comprar: {
                    label: 'Ok',
                    className: 'btn-primary',
                    callback: function() {
                        if(!$("#project-form").validate().form()){
                            return false;
                        }

                        var type = verb;
                        var url = urlForm
                        var formdata = null;
                        var contentType = "application/x-www-form-urlencoded";
                        var processData = true;
                        if ($("#project-form").attr('enctype') === 'multipart/form-data') {
                            formdata = new FormData($("#project-form").get(0));//seleciona classe form-horizontal adicionada na tag form do html
                            contentType = false;
                            processData = false;
                        } else {
                            formdata = $("#project-form").serialize();
                        }

                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:"json",
                            type: type,
                            url: url,
                            data: formdata, // serializes the form's elements.
                            contentType: contentType,
                            processData: processData,
                            cache: false,
                            success: function(data){
                                bootbox.hideAll();
                                table.ajax.reload();
                            },
                            error: function(data){

                            }
                        });
                        return false;
                    }
                },
                cancelar: {
                    label: 'Cancelar',
                    className: 'btn-danger',
                    callback: function() {
                        return true;
                    }
                }
            }
        });
    });

}

function removeItem(id)
{
    bootbox.confirm({
        message: "Deseja mesmo excluir esse projeto?",
        buttons: {
            confirm: {
                label: 'Sim',
                className: 'btn-success'
            },
            cancel: {
                label: 'Não',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                var type = "DELETE";
                var url = "/project/delete/"+id;
                var formdata = null;
                var contentType = "application/x-www-form-urlencoded";
                var processData = true;
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:"json",
                    type: type,
                    url: url,
                    data: formdata, // serializes the form's elements.
                    contentType: contentType,
                    processData: processData,
                    cache: false,
                    success: function(data){
                        bootbox.hideAll();
                        table.ajax.reload();
                    },
                    error: function(data){

                    }
                });
            }
        }
    });
}

function viewLDJson(id)
{
    $.get( "/project/view-ldjson", {cache: false, "id": id},function( data ) {
        bootbox.dialog({
            title: "LD-Json Gerado",
            message: data,
            closeButton: true,
            scrollable: true,
            size: 'large',
            buttons: {
                copy_and_paste: {
                    label: 'Copiar Script',
                    className: 'btn-primary',
                    callback: function() {
                        (async () => {
                            await navigator.clipboard.writeText($("#ldjson-form #ld_json").text());
                        })();
                    }
                },
                cancelar: {
                    label: 'Cancelar',
                    className: 'btn-danger',
                    callback: function() {
                        return true;
                    }
                }
            }
        }).find("div.modal-dialog").css({ "width": "100%", "height": "100%" });
    });
}

var table;
$(document).ready( function () {
    table = $('#table_id').DataTable({
        buttons: [
            {
                text: 'Novo projeto',
                action: function ( e, dt, node, config ) {
                    $.get( "/project/check-create", {cache: false}, function( data ) {
                        if (data.success) {
                            getForm("");
                        } else {
                            bootbox.alert({
                                message: data.message,
                                locale:"pt"
                            });
                        }

                    }, "json");

                }
            }
        ],
        dom: 'Bfrtip',
        lengthChange: false,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Portuguese-Brasil.json"
        },
        processing: true,
        serverSide: true,
        ajax: {
            url:'/project/get-list',
            "type": "POST",
            "beforeSend" : function(xhr) {
                var token = $('meta[name="csrf-token"]').attr('content');
                xhr.setRequestHeader('X-CSRF-TOKEN',token);
            },
            data: function (d) {
                //d.razao_social = $('#frm-filter #razao_social').val();
            }
        },

        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'url', name: 'url'},
            {data: 'acao', name: 'acao'},
        ],
        "order": [[ 1, 'asc' ]],
        "fnDrawCallback": function (data, type, full, meta) {

        }
    });
    table.buttons().container().appendTo( '#table_id_wrapper .col-md-6:eq(0)' );

    $(document).off('click', '.editar');
    $(document).on('click', '.editar', function () {
        getForm($(this).data('id'));
    });


    $(document).off('click', '.remover');
    $(document).on('click', '.remover', function () {
        removeItem($(this).data('id'));
    });

    $(document).off('click', '.ld_json_viewer');
    $(document).on('click', '.ld_json_viewer', function () {
        viewLDJson($(this).data('id'));
    });
} );
