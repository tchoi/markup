function getFormUser(id)
{
    var verb = "POST";
    var urlForm = "/user/store"

    if (id != "") {
        verb = "PUT";
        urlForm = "/user/edit/" + id
    }
    $.get( "/user/form", {cache: false, "id": id},function( data ) {
        bootbox.dialog({
            title: "Cadastro de usuário",
            message: data,
            closeButton: true,
            scrollable: true,
            size: 'large',
            buttons: {
                comprar: {
                    label: 'Ok',
                    className: 'btn-primary',
                    callback: function() {
                        if(!$("#user-form").validate().form()){
                            return false;
                        }

                        var type = verb;
                        var url = urlForm
                        var formdata = null;
                        var contentType = "application/x-www-form-urlencoded";
                        var processData = true;
                        if ($("#user-form").attr('enctype') === 'multipart/form-data') {
                            formdata = new FormData($("#user-form").get(0));//seleciona classe form-horizontal adicionada na tag form do html
                            contentType = false;
                            processData = false;
                        } else {
                            formdata = $("#user-form").serialize();
                        }

                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:"json",
                            type: type,
                            url: url,
                            data: formdata, // serializes the form's elements.
                            contentType: contentType,
                            processData: processData,
                            cache: false,
                            success: function(data){
                                bootbox.hideAll();
                                if (tableUser != undefined) {
                                    tableUser.ajax.reload();
                                }

                            },
                            error: function(data){

                            }
                        });
                        return false;
                    }
                },
                cancelar: {
                    label: 'Cancelar',
                    className: 'btn-danger',
                    callback: function() {
                        return true;
                    }
                }
            }
        });
    });

}
$(document).ready(function(){
    $('.cep').mask('00000-000');

    $(document).off('click', '.edit-profile');
    $(document).on('click', '.edit-profile', function () {
        getFormUser($(this).data('id'));
    })
});
