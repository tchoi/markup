<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application Services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application Services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            // Add some items to the menu...
            $user = Auth::user();
            $event->menu->add(["header" => 'NAVEGAÇÃO']);
            $event->menu->add([
                'text' => "Projetos",
                'url' => 'project/list',
                'icon' => 'fas fa-fw fa-project-diagram',
            ]);
            $event->menu->add([
                'text' => "Marcadores",
                'url' => 'marker/list',
                'icon' => 'fas fa-fw fa-marker',
            ]);
            $event->menu->add([
                'text' => 'profile',
                'url'  => '#',
                'icon' => 'fas fa-fw fa-user',
                'classes' => "edit-profile",
                "data" => [
                    'id' => $user->id
                ]
            ]);
            if ($user->level == "ADMIN") {
                $event->menu->add(["header" => 'CONTROLE DE USUÁRIOS']);
                $event->menu->add([
                    'text' => 'Usuários',
                    'url'  => 'user/list',
                    'icon' => 'fas fa-fw fa-users',
                ]);
            }
        });
    }
}
