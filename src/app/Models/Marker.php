<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Marker extends Model
{
    use HasFactory;

    protected $fillable = [
        "project_id",
        "type",
        "name",
        "description",
        "url",
        "in_language",
        "date_published",
        "date_modified",
        "breadcrumbs",
        "faqs",
        'image',
        'width',
        'height',
        'caption'
    ];
}
