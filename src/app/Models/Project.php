<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'description',
        'url',
        'same_as',
        'in_language',
        'organization_area',
        'website_area',
        'webpage_area',
        'breadcrumb_area',
        'logo',
        'legal_name',
        'founding_date',
        'founders',
        'address',
        'contact_point'
    ];
}
