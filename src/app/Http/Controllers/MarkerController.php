<?php

namespace App\Http\Controllers;

use App\Services\MarkerService;
use App\Services\ProjectService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MarkerController extends Controller
{
    protected MarkerService $markerService;
    protected ProjectService $projectService;

    /**
     * MarkerController constructor.
     * @param MarkerService $markerService
     * @param ProjectService $projectService
     */
    public function __construct(MarkerService $markerService, ProjectService $projectService)
    {
        $this->markerService = $markerService;
        $this->projectService = $projectService;
    }


    public function index(Request $request)
    {
        return view('marker.list', []);
    }

    public function getList(Request $request)
    {
        return $this->markerService->list();
    }

    public function form(Request $request)
    {
        $reg = [];
        $type = $request->get("type") ?? "";
        if ($request->has("id") && !empty($request->get("id"))) {
            $reg = $this->markerService->getOne($request->get("id"));
            $type = $reg['type'];
        }
        $reg['projects'] = $this->projectService->getAllProjects();
        Log::info(json_encode($reg));
        return view("marker.form_" . strtolower($type), $reg);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $resp = $this->markerService->store($data);
        if ($resp === false) {
            return response()->json(["message" => "Erro ao tentar salvar marcador"], 403);
        }

        return response()->json(["message" => "Marcador salvo com sucesso"], 201);
    }

    public function edit(Request $request, $id)
    {
        $data = $request->all();
        $resp = $this->markerService->edit($id, $data);
        if ($resp === false) {
            return response()->json(["message" => "Erro ao tentar salvar marcador"], 403);
        }

        return response()->json(["message" => "Marcador salvo com sucesso"], 201);
    }

    public function delete(Request $request, $id)
    {
        $resp = $this->markerService->delete($id);
        if ($resp === false) {
            return response()->json(["message" => "Erro ao tentar excluir marcador"], 403);
        }

        return response()->json(["message" => "Marcador excluido com sucesso"], 200);
    }

    public function getLDJson(Request $request)
    {
        $json = $this->markerService->generateJson($request->get("id"));
        return view('ldjson', [
            "json" => $json
        ]);
    }

}
