<?php

namespace App\Http\Controllers;

use App\Services\ProjectService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProjectController extends Controller
{
    protected ProjectService $projectService;
    protected UserService $userService;

    /**
     * ProjectController constructor.
     * @param ProjectService $projectService
     * @param UserService $userService
     */
    public function __construct(ProjectService $projectService, UserService $userService)
    {
        $this->projectService = $projectService;
        $this->userService = $userService;
    }


    public function index(Request $request)
    {
        return view('project.list', []);
    }

    public function getList(Request $request)
    {
        return $this->projectService->list();
    }

    public function form(Request $request)
    {
        $reg = [];
        if ($request->has("id") && !empty($request->get("id"))) {
            $reg = $this->projectService->getOne($request->get("id"));
        }
        $reg['users'] = $this->userService->getAllUsers();
        return view('project.form', $reg);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $resp = $this->projectService->store($data);
        if ($resp === false) {
            return response()->json(["message" => "Erro ao tentar salvar projeto"], 403);
        }

        return response()->json(["message" => "projeto salvo com sucesso"], 201);
    }

    public function edit(Request $request, $id)
    {
        $data = $request->all();
        $resp = $this->projectService->edit($id, $data);
        if ($resp === false) {
            return response()->json(["message" => "Erro ao tentar salvar projeto"], 403);
        }

        return response()->json(["message" => "Projeto salvo com sucesso"], 201);
    }

    public function delete(Request $request, $id)
    {
        $resp = $this->projectService->delete($id);
        if ($resp === false) {
            return response()->json(["message" => "Erro ao tentar excluir projeto"], 403);
        }

        return response()->json(["message" => "Projeto excluido com sucesso"], 200);
    }

    public function checkIfPermitProject()
    {
        $resp = $this->projectService->qtdeProjects();
        if ($resp === false) {
            return response()->json(
                [
                    "message" => "Solicite liberação com os administradores",
                    "success" => false
                ],
                200
            );
        }

        return response()->json(["message" => "Criação de projetos liberado", "success" => true], 200);
    }

    public function getLDJson(Request $request)
    {
        $json = $this->projectService->generateJson($request->get("id"));
        return view('ldjson', [
            "json" => $json
        ]);
    }
}
