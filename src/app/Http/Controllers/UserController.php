<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected UserService $userService;

    /**
     * UserController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }


    public function index(Request $request)
    {
        return view('user.list', []);
    }

    public function getList(Request $request)
    {
        return $this->userService->list();
    }

    public function form(Request $request)
    {
        $user = [];
        if ($request->has("id") && !empty($request->get("id"))) {
            $user = $this->userService->getOne($request->get("id"));
        }
        return view('user.form', $user);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $resp = $this->userService->store($data);
        if ($resp === false) {
            return response()->json(["message" => "Erro ao tentar salvar usuario"], 403);
        }

        return response()->json(["message" => "Usuario salvo com sucesso"], 201);
    }

    public function edit(Request $request, $id)
    {
        $data = $request->all();
        $resp = $this->userService->edit($id, $data);
        if ($resp === false) {
            return response()->json(["message" => "Erro ao tentar salvar usuario"], 403);
        }

        return response()->json(["message" => "Usuario salvo com sucesso"], 201);
    }

    public function delete(Request $request, $id)
    {
        $resp = $this->userService->delete($id);
        if ($resp === false) {
            return response()->json(["message" => "Erro ao tentar excluir usuario"], 403);
        }

        return response()->json(["message" => "Usuario excluido com sucesso"], 200);
    }
}
