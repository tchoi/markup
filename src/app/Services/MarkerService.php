<?php

namespace App\Services;

use App\Models\Marker;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class MarkerService
{
    public const TYPE_WEBPAGE = "WEBPAGE";
    public const TYPE_FAQ = "FAQ";
    public const TYPES = [
        self::TYPE_WEBPAGE => "WebPage",
        self::TYPE_FAQ => "FAQ"
    ];
    protected Marker $model;
    protected ProjectService $projectService;
    protected SchemaService $schemaService;

    /**
     * MarkerService constructor.
     * @param Marker $model
     * @param ProjectService $projectService
     * @param SchemaService $schemaService
     */
    public function __construct(Marker $model, ProjectService $projectService, SchemaService $schemaService)
    {
        $this->model = $model;
        $this->projectService = $projectService;
        $this->schemaService = $schemaService;
    }


    public function list()
    {
        $auth = Auth::user();
        $query = $this->model->whereRaw("1 = 1")->select(
            "markers.*",
            "projects.name as project_name"
        );
        $query->join("projects", "projects.id", "=", "markers.project_id");
        $query->join("users", "users.id", "=", "projects.user_id");
        if ($auth->level == UserService::LEVEL_USER) {
            $query->where("users.id", $auth->id);
        }
        $query->orderBy("projects.name", "ASC");
        $regs = $query->get();
        return DataTables::of($regs)
            ->addColumn('acao', function (Marker $obj) use ($auth) {
                $aButtons = [];
                $aButtons[] = "<button class='btn btn-default editar' data-id='{$obj->id}' data-typemarker='{$obj->type}' href='javascript:void();'><i class='fas fa-edit'></i> Editar</button>";
                $aButtons[] = "<button class='btn btn-warning remover' data-id='{$obj->id}' href='javascript:void();'><i class='fas fa-times'></i> Remover</button>";
                $aButtons[] = "<button class='btn btn-default ld_json_viewer' data-id='{$obj->id}' href='javascript:void();'><i class='fas fa-search'></i> Ver LD-JSON</button>";
                return implode(" ", $aButtons);
            })
            ->rawColumns(['acao'])
            ->make(true);
    }

    public function getOne($id)
    {
        $query = $this->model->where("id", $id);
        return $query->first()->toArray();
    }

    public function store(array $data)
    {
        $save = new Marker();
        $save->fill($data);
        return $save->save() ? $save : false;
    }

    public function edit($id, array $data)
    {
        $save = $this->model->find($id);
        $save->fill($data);
        return $save->save() ? $save : false;
    }

    public function delete($id)
    {
        $save = $this->model->find($id);
        return $save->delete();
    }

    public function generateJson($id)
    {
        $marker = $this->getOne($id);
        $aJson = $this->projectService->generateJson($marker['project_id'], false);
        if ($marker['type'] == self::TYPE_WEBPAGE) {
            $webPageArray = [
                'url' => $marker['url'],
                'name' => $marker['name'],
                'description' => $marker['description'],
                'url_organization' => $marker['url'],
                'in_language' => $marker['in_language'],
                'url_website' => $marker['url'],
                'date_published' => Carbon::now()->format("c"),
                'date_modified' => Carbon::now()->format("c"),
            ];

            if (isset($marker['image']) && !empty($marker['image'])) {
                $webPageArray['primary_image_of_image'] = $marker['url'];
            }

            $aJson[] = $this->schemaService->makeWebPageJson($webPageArray, false);

            if (isset($marker['image']) && !empty($marker['image'])) {
                $aJson[] = $this->schemaService->makeImagePrimaryJson([
                    'url' => $marker['url'],
                    'in_language' => $marker['in_language'],
                    'image' => $marker['image'],
                    'width' => $marker['width'],
                    'height' => $marker['height'],
                    'caption' => $marker['caption'],
                ], false);
            }

            $breadCrumbs = explode("\n", $marker["breadcrumbs"]);
            $itemsListElement = [];
            $pos = 2;
            foreach ($breadCrumbs as $breadCrumb) {
                if (empty($breadCrumb)) {
                    continue;
                }
                $breadCrumb = explode(",", $breadCrumb);
                $itemsListElement[] = [
                    '@type' => "ListItem",
                    'position' => $pos,
                    'name' => $breadCrumb[0],
                    'item' => $breadCrumb[1]
                ];
                $pos++;
            }
            $itemsListElement[] = [
                '@type' => "ListItem",
                'position' => $pos,
                'name' => $marker['name'],
                'item' => $marker['url']
            ];
            $aJson[] = $this->schemaService->makeBreadcrumbJson([
                'url' => $marker['url'],
                'item_list_element' => $itemsListElement
            ], false);
        } else {
            $faqs = explode("\n", $marker["faqs"]);
            $aList = [];
            foreach ($faqs as $faq) {
                $faq = explode("@@", $faq);
                $aList[] = [
                    "@type" => "Question",
                    "name" => $faq[0],
                    "acceptedAnswer" => [
                        "@type" => "Awswer",
                        "text" => $faq[1]
                    ]
                ];
            }

            $aJson[] = $this->schemaService->makeFaqJson([
                "faqs" => $aList
            ], false);
        }

        return str_replace("\/", "/", $this->schemaService->makeAllJson($aJson));
    }
}
