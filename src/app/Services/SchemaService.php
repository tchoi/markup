<?php

namespace App\Services;

class SchemaService
{
    public function makeOrganizationJson(array $data, bool $returnJson = true)
    {
        $json = [
            '@type' => "Organization",
            '@id' => "{$data['url']}#organization",
            'name' => $data['name'],
            'legalName' => $data['legal_name'],
            'foundingDate' => $data['founding_date'],
            'url' => $data['url'],
            'logo' => $data['logo'],
            'sameAs' => $data['same_as'],
        ];
        if (count($data['founders']) > 0) {
            $json['founders'] = $data['founders'];
        }

        if (count($data['address']) > 0) {
            $json['address'] = [
                '@type' => "PostalAddress",
                'streetAddress' => $data['address']['street'],
                'addressLocality' => $data['address']['city'],
                'addressRegion' => $data['address']['state'],
                'postalCode' => $data['address']['postalcode'],
                'addressCountry' => $data['address']['country'],
            ];
        }

        if (count($data['contact_point']) > 0) {
            $json['contactPoint'] = [
                '@type' => "ContactPoint",
                'contactType' => $data['contact_point']['type'],
                'email' => $data['contact_point']['email'],
            ];
            if (!empty($data['contact_point']['telephone'])) {
                $json['contactPoint']['telephone'] = $data['contact_point']['telephone'];
            }
        }

        return $returnJson ? json_encode($json, JSON_PRETTY_PRINT) : $json;
    }

    public function makeWebsiteJson(array $data, bool $returnJson = true)
    {
        $json = [
            '@type' => "Website",
            '@id' => "{$data['url']}#website",
            'name' => $data['name'],
            'url' => $data['url'],
            "description" => $data['description'],
            'publisher' => [
                "@id" => "{$data['url_organization']}#organization"
            ],
            'inLanguage' => $data['in_language']
        ];

        return $returnJson ? json_encode($json, JSON_PRETTY_PRINT) : $json;
    }

    public function makeWebPageJson(array $data, bool $returnJson = true)
    {
        $json = [
            '@type' => "WebPage",
            '@id' => "{$data['url']}#webpage",
            'url' => $data['url'],
            'name' => $data['name'],
            "description" => $data['description'],
            'isPartOf' => [
                "@id" => "{$data['url_website']}#website"
            ],
            "about" => [
                "@id" => "{$data['url_organization']}#organization"
            ],
            "datePublished" => $data['date_published'],
            "dateModified" => $data['date_modified'],
            'inLanguage' => $data['in_language'],
            "breadcrumb" => [
                "@id" => "{$data['url']}#breadcrumb"
            ]
        ];
        if (isset($data['primary_image_of_image'])) {
            $json['primaryImageOfPage']["@id"] = $data['primary_image_of_image'] . "#primary_image";
        }
        return $returnJson ? json_encode($json, JSON_PRETTY_PRINT) : $json;
    }

    public function makeBreadcrumbJson(array $data, bool $returnJson = true)
    {
        $json = [
            '@type' => "BreadcrumbList",
            '@id' => "{$data['url']}#breadcrumb",
            'itemListElement' => $data['item_list_element']
        ];

        return $returnJson ? json_encode($json, JSON_PRETTY_PRINT) : $json;
    }

    public function makeFaqJson(array $data, bool $returnJson = true)
    {
        $json = [
            "@type" => "FAQPage",
            "mainEntity" => $data['faqs']
        ];
        return $returnJson ? json_encode($json, JSON_PRETTY_PRINT) : $json;
    }

    public function makeImagePrimaryJson(array $data, bool $returnJson = true)
    {
        $json = [
            "@type" => "ImageObject",
            "@id" => $data['url'] . "#primaryimage",
            "inLanguage" => $data['in_language'],
            "url" => $data['image'],
            "contentUrl" => $data['image'],
            "width" => $data['width'],
            "height" => $data['height'],
            "caption" => $data['caption']
        ];
        return $returnJson ? json_encode($json, JSON_PRETTY_PRINT) : $json;
    }

    public function makeAllJson(array $data)
    {
        $json = [
            "@context" => "https://schema.org",
            "@graph" => $data
        ];

        return json_encode($json, JSON_PRETTY_PRINT);
    }
}
