<?php


namespace App\Services;


use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class UserService
{
    public const LEVEL_ADMIN = "ADMIN";
    public const LEVEL_USER = "USER";
    public function list()
    {
        $auth = Auth::user();
        $query = User::whereRaw("1 = 1");
        $regs = $query->get();


        return DataTables::of($regs)
            ->addColumn('acao', function(User $obj) use ($auth) {
                $aButtons = [];
                if (in_array($auth->level, [UserService::LEVEL_ADMIN])) {
                    $aButtons[] = "<button class='btn btn-default editar' data-id='{$obj->id}' href='javascript:void();'><i class='fas fa-edit'></i> Editar</button>";
                }
                if ($auth->level == UserService::LEVEL_ADMIN) {
                    $aButtons[] = "<button class='btn btn-warning remover' data-id='{$obj->id}' href='javascript:void();'><i class='fas fa-times'></i> Remover</button>";
                }

                return implode(" ", $aButtons);
            })
            ->rawColumns(['acao'])
            ->make(true);
    }

    public function getOne($id)
    {
        $query = User::where("id", $id);
        return $query->first()->toArray();
    }

    public function store(array $data)
    {
        $data['password'] = Hash::make($data['password']);
        $data["email_verified_at"] = now();
        $data["remember_token"] = Str::random(10);

        $user = new User();
        $user->fill($data);
        return $user->save() ? $user : false;
    }

    public function edit($id, array $data)
    {
        if (isset($data['password']) && !empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }

        $user = User::find($id);
        $user->fill($data);
        return $user->save() ? $user : false;
    }

    public function delete($id)
    {
        $user = User::find($id);
        return $user->delete();
    }

    public function getAllUsers()
    {
        $query = User::whereRaw("1=1")->orderBy("name", "ASC");
        return $query->get();
    }

}
