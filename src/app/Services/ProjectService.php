<?php

namespace App\Services;

use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class ProjectService
{
    protected Project $model;
    protected SchemaService $schemaService;

    /**
     * ProjectService constructor.
     * @param Project $model
     * @param SchemaService $schemaService
     */
    public function __construct(Project $model, SchemaService $schemaService)
    {
        $this->model = $model;
        $this->schemaService = $schemaService;
    }


    public function list()
    {
        $auth = Auth::user();
        $query = $this->model->whereRaw("1 = 1");
        if ($auth->level == UserService::LEVEL_USER) {
            $query->where("user_id", $auth->id);
        }
        $regs = $query->get();


        return DataTables::of($regs)
            ->addColumn('acao', function (Project $obj) use ($auth) {
                $aButtons = [];
                $aButtons[] = "<button class='btn btn-default editar' data-id='{$obj->id}' href='javascript:void();'><i class='fas fa-edit'></i> Editar</button>";

                if ($auth->level == UserService::LEVEL_ADMIN) {
                    $aButtons[] = "<button class='btn btn-warning remover' data-id='{$obj->id}' href='javascript:void();'><i class='fas fa-times'></i> Remover</button>";
                }

                $aButtons[] = "<button class='btn btn-default ld_json_viewer' data-id='{$obj->id}' href='javascript:void();'><i class='fas fa-search'></i> Ver LD-JSON</button>";

                return implode(" ", $aButtons);
            })
            ->rawColumns(['acao'])
            ->make(true);
    }

    public function getOne($id)
    {
        $query = $this->model->where("id", $id);
        return $query->first()->toArray();
    }

    public function store(array $data)
    {
        $data["same_as"] = explode("/n", $data['same_as']);
        $data['address'] = json_encode($data['address']);
        $data['contact_point'] = json_encode($data['contact_point']);

        $data["same_as"] = implode("\n", $data["same_as"]);
        $reg = new Project();
        $reg->fill($data);
        return $reg->save() ? $reg : false;
    }

    public function edit($id, array $data)
    {
        $data["same_as"] = explode("/n", str_replace("\r", "", $data['same_as']));
        $data['address'] = json_encode($data['address']);
        $data['contact_point'] = json_encode($data['contact_point']);

        $data["same_as"] = implode("\n", $data["same_as"]);
        $reg = $this->model->find($id);
        $reg->fill($data);
        return $reg->save() ? $reg : false;
    }

    public function delete($id)
    {
        $reg = $this->model->find($id);
        return $reg->delete();
    }

    public function qtdeProjects()
    {
        $auth = Auth::user();
        $query = $this->model->whereRaw("1 = 1");
        if ($auth->level == UserService::LEVEL_USER) {
            $query->where("user_id", $auth->id);
        }
        $regs = $query->get();
        if ($auth->level == UserService::LEVEL_USER) {
            if (count($regs) < $auth->qtd_project) {
                return true;
            }
            return false;
        }
        return true;
    }

    public function getAllProjects()
    {
        $auth = Auth::user();
        $query = $this->model->whereRaw("1 = 1");
        if ($auth->level == UserService::LEVEL_USER) {
            $query->where("user_id", $auth->id);
        }
        return $query->orderBy("name", "ASC")->get();
    }

    public function generateJson($id, $returnJson = true)
    {
        $aJson = [];
        $data = $this->getOne($id);
        $sameAs = explode("\n", str_replace("\r", "", $data['same_as']));
        $aFounders = [];
        $founders = explode("\n", str_replace("\r", "", $data['founders']));
        foreach ($founders as $founder) {
            if (empty($founder)) {
                continue;
            }
            $aFounders[] = [
                '@type' => "Person",
                'name' => $founder
            ];
        }

        $address = !empty($data['address']) ? json_decode($data['address'], true) : [];
        $contactPoint = !empty($data['contact_point']) ? json_decode($data['contact_point'], true) : [];

        $aJson[] = $this->schemaService->makeOrganizationJson([
            'name' => $data['name'],
            'legal_name' => $data['legal_name'],
            'founding_date' => $data['founding_date'],
            'url' => $data['url'],
            'same_as' => $sameAs,
            'logo' => $data['logo'],
            'founders' => $aFounders,
            'address' => $address,
            'contact_point' => $contactPoint,
        ], false);

        $aJson[] = $this->schemaService->makeWebsiteJson([
            'url' => $data['url'],
            'name' => $data['name'],
            'description' => $data['description'],
            'url_organization' => $data['url'],
            'in_language' => $data['in_language']
        ], false);

        $aJson[] = $this->schemaService->makeWebPageJson([
            'url' => $data['url'],
            'name' => $data['name'],
            'description' => $data['description'],
            'url_organization' => $data['url'],
            'in_language' => $data['in_language'],
            'url_website' => $data['url'],
            'date_published' => Carbon::now()->format("c"),
            'date_modified' => Carbon::now()->format("c"),

        ], false);

        $aJson[] = $this->schemaService->makeBreadcrumbJson([
            'url' => $data['url'],
            'item_list_element' => [
                [
                    '@type' => "ListItem",
                    'position' => 1,
                    'name' => "Início"
                ]
            ]
        ], false);

        return $returnJson ?
            str_replace("\/", "/", $this->schemaService->makeAllJson($aJson))
            : $aJson;
    }
}
